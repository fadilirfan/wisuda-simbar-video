<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\Video;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\FileUploadController;
use App\Http\Controllers\CaptchaServiceController;
 
Route::get('/', [FileUploadController::class, 'index']);
Route::post('store', [FileUploadController::class, 'store']);

Route::get('/contact-form', [CaptchaServiceController::class, 'index']);
Route::post('/captcha-validation', [CaptchaServiceController::class, 'capthcaFormValidate']);
Route::get('/reload-captcha', [CaptchaServiceController::class, 'reloadCaptcha']);


/* Route::get('/', function () {
    return view('welcome');
}); */

Route::get('getProdi/{id}', function ($id) {
    $singkatan=App\Models\Sekolah::where('singkatan',$id)->pluck('id');
    //dd($singkatan);
    $prodi = App\Models\Prodi::where('sekolah_id',$singkatan)->get();
    return response()->json($prodi);
});

Route::get('getWisuda', function (Request $request) {
    if ($request->ajax()) {
            $data = Video::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'. route('wisuda.lihat' , $row->id).'" class="edit btn btn-info btn-sm"> <i class="fa fa-eye" aria-hidden="true"></i> Lihat</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
})->name('user.index'); 

Route::get('/wisudawan', function () {
    return view('wisuda');
}); 

Route::get('lihat/{id}',[FileUploadController::class, 'lihat'])->name('wisuda.lihat');


Route::get('/room1gladiwisuda39ptsastmiksumedang', [FileUploadController::class, 'gladiroom1']);


Route::get('/room2gladiwisuda39ptsastmiksumedang', [FileUploadController::class, 'gladiroom2']);

Route::get('/room1wisuda39ptsastmiksumedang', [FileUploadController::class, 'wisudaroom1']);

Route::get('/room2wisuda39ptsastmiksumedang', [FileUploadController::class, 'wisudaroom2']);

Route::get('/roomtamuwisuda39ptsastmiksumedang', [FileUploadController::class, 'roomtamu']);
