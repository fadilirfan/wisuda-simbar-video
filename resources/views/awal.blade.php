<!DOCTYPE html>
<html>
<head>
  <title>Wisuda 39 PTSA dan STMIK Sumedang - Upload Video Wisudawan</title>
  <link rel="apple-touch-icon" sizes="57x57" href="{{ url('fav/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ url('fav/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ url('fav/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ url('fav/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ url('fav/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ url('fav/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ url('fav/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ url('fav/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ url('fav/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ url('fav/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ url('fav/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ url('fav/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ url('fav/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ url('fav/manifest.json') }}"> 
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ url('fav/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- Styles -->
    <style>
        .container {
            max-width: 100%;
        }
        .reload {
            font-family: Lucida Sans Unicode
        }
        .masthead {
            height: 100vh;
            min-height: 300px;
            background-image: url('/header.png');
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            }   
    </style>

</head>
<body>

<!-- Full Page Image Header with Vertically Centered Content -->
<header class="masthead">
  <div class="container h-100">
    <div class="row h-100 align-items-center">
    </div>
  </div>
</header>
 
<div class="container mt-2">
<br/>
  <br/>
  <h2 class="text-center">Form Upload Video Wisudawan</h2>
  <br/>
  @include('message')
  <br/>

  @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
      <form method="POST" enctype="multipart/form-data" id="upload-file" action="{{ url('store') }}" >
      @csrf
          <div class="row">
          <div class="col-md-12">
                  <div class="form-group mb-4">
                  <label for="exampleFormControlSelect1">Masukan No Urut</label>
                      <input type="text" name="no_urut" class="form-control" placeholder="Masukan No Urut" id="no_urut" required autofocus>
                  </div>
              </div>

              <div class="col-md-12">
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Masukan Nama Wisudawan</label>
                      <input type="text" name="nama" class="form-control" placeholder="Masukan Nama" id="nama" required>
                  </div>
              </div>

              <div class="col-md-12">
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Pilih Sekolah Tinggi</label>
                  <select class="form-control" name="sekolah_tinggi" id="sekolah">
                            <option hidden>Pilih Sekolah Tinggi</option>
                            @foreach ($sekolah as $item)
                            <option value="{{ $item->singkatan }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        
                  </div>
              </div>

              <div class="col-md-12">

                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Pilih Program Studi</label>
                    <select class="form-control" name="prodi" id="prodi"></select>
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Upload Video Simbar</label>
                      <input type="file" name="file" placeholder="Choose file" class="form-control" id="file">
                        @error('file')
                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                        @enderror
                  </div>
              </div>
              <div class="col-md-12">
              <div class="form-group mt-4 mb-4">
                <div class="captcha">
                    <span>{!! captcha_img() !!}</span>
                    <button type="button" class="btn btn-danger" class="reload" id="reload">
                        &#x21bb;
                    </button>
                </div>
            </div>

            <div class="form-group mb-4">
                <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
            </div>
              </div>
              <div class="col-md-12">
                  <button type="submit" class="btn btn-primary" id="submit"> <i class="fa fa-paper-plane" aria-hidden="true"></i> Kirim</button>
              </div>
          </div>     
      </form>
</div>
<br/>
<br/>


<!-- Footer -->
<footer class="page-footer font-small pt-4" style="background-color:#2ea9e1;color:white">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">Wisuda 39</h5>
        <p>PTSA dan STMIK Sumedang </p>
        <p>Seksi Acara dan Tim IT </p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Kontak Person</h5>

        <ul class="list-unstyled">
          <li>
            <a href="https://wa.me/085220717928" style="color:white;">Whatsapp</a>
          </li>
          <li>
            <a href="mailto:fadilirfan@stmik-sumedang.ac.id" style="color:white;">Email</a>
          </li>
          
        </ul>

      </div>
      <!-- Grid column -->


      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Menu Aplikasi</h5>

        <ul class="list-unstyled">
          <li>
            <a href="{{ url('/') }}" style="color:white;">Form Upload Simbar Wisuda</a>
          </li>
          <li>
            <a href="{{ url('/wisudawan') }}" style="color:white;">Data Wisudawan</a>
          </li>
          
        </ul>

      </div>
      <!-- Grid column -->

     

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
<div class="footer-copyright text-center py-3">© 2021 Copyright:
  <a href="#" style="color:white;"> PTSA dan STMIK Sumedang</a>
</div>
<!-- Copyright -->

</footer>
<!-- Footer -->
 
</div> 
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function() {
            $('#sekolah').on('change', function() {
               var sekolahID = $(this).val();
               if(sekolahID) {
                   $.ajax({
                       url: '/getProdi/'+sekolahID,
                       type: "GET",
                       data : {"_token":"{{ csrf_token() }}"},
                       dataType: "json",
                       success:function(data)
                       {
                         if(data){
                            $('#prodi').empty();
                            $('#prodi').append('<option hidden>Pilih Prodi</option>'); 
                            $.each(data, function(key, prodi){
                                $('select[name="prodi"]').append('<option value="'+ prodi.name +'">' + prodi.name+ '</option>');
                            });
                        }else{
                            $('#prodi').empty();
                        }
                     }
                   });
               }else{
                 $('#prodi').empty();
               }
            });
            });
        </script> 
</body>
<script type="text/javascript">
    $('#reload').click(function () {
        $.ajax({
            type: 'GET',
            url: 'reload-captcha',
            success: function (data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });

</script>
</html>