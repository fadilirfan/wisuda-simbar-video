<!DOCTYPE html>
<html lang="id">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Wisuda 39 PTSA dan STMIK Sumedang - Upload Video Wisudawan</title>
  <link rel="apple-touch-icon" sizes="57x57" href="{{ url('fav/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ url('fav/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ url('fav/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ url('fav/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ url('fav/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ url('fav/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ url('fav/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ url('fav/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ url('fav/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ url('fav/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ url('fav/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ url('fav/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ url('fav/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ url('fav/manifest.json') }}"> 
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ url('fav/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script></head>

<style>
        .container {
            max-width: 100%;
        }

        .formcontain {
            max-width: 500px;
            margin: 0 auto;
        }
        .reload {
            font-family: Lucida Sans Unicode
        }
    </style>
<body>
<div class="container">
    <div class="jumbotron" style="margin-bottom:10px;">
    <img class="img-fluid" src="{{ url('/header.png'); }}" alt="Wisuda 39 PTSA dan STMIK Sumedang"> 
    </div>
    <div class="formcontain center">
    <div class="row">
    <h2 class="text-center">Lihat Data Video Wisudawan</h2>
  <br/>
  <br/>

          <div class="row">
          <div class="col-md-12">
                  <div class="form-group mb-4">
                  <label for="exampleFormControlSelect1">No. Urut</label>
                      <input type="text" class="form-control" value="{{ $video->no_urut }}" readonly autofocus>
                  </div>
              </div>

              <div class="col-md-12">
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Nama Wisudawan</label>
                      <input type="text" class="form-control" value="{{ $video->nama }}" readonly>
                  </div>
              </div>

              <div class="col-md-12">
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Sekolah Tinggi</label>
                  <input type="text" class="form-control" value="{{ $video->sekolah_tinggi }}" readonly>
                  
                        
                  </div>
              </div>

              <div class="col-md-12">

                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Program Studi</label>
                  <input type="text" class="form-control" value="{{ $video->jurusan }}" readonly>
                   
                  </div>
              </div>
              <div class="col-md-12">
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">Video Simbar</label>
                  <p style="font-size:18px;color:red;">Pastikan Video Simbar dapat di jalankan di form ini<br/> Jika Tidak Berjalan Lakukan Pengumpulan ulang</p>
                  <br/>
                  <video width="100%" height="240" controls autoplay>
                    <source src="{{ url(str_replace('public','storage',$video->path))}}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                     
                  </div>
              </div>
             
              <div class="col-md-12">
                  <button onclick="history.back()" class="btn btn-primary" > <i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Kembali</button>
              </div>
          </div> 
    
    </div>
    <hr>
  </div>
</div>



<!-- Footer -->
<footer class="page-footer font-small pt-4" style="background-color:#2ea9e1;color:white">

  <!-- Footer Links -->
  <div class="container-fluid text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">Wisuda 39</h5>
        <p>PTSA dan STMIK Sumedang </p>
        <p>Seksi Acara dan Tim IT </p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Kontak Person</h5>

        <ul class="list-unstyled">
          <li>
            <a href="https://wa.me/085220717928" style="color:white;">Whatsapp</a>
          </li>
          <li>
            <a href="mailto:fadilirfan@stmik-sumedang.ac.id" style="color:white;">Email</a>
          </li>
          
        </ul>

      </div>
      <!-- Grid column -->


      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Menu Aplikasi</h5>

        <ul class="list-unstyled">
          <li>
            <a href="{{ url('/') }}" style="color:white;">Form Upload Simbar Wisuda</a>
          </li>
          <li>
            <a href="{{ url('/wisudawan') }}" style="color:white;">Data Wisudawan</a>
          </li>
          
        </ul>

      </div>
      <!-- Grid column -->

     

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
<div class="footer-copyright text-center py-3">© 2021 Copyright:
  <a href="#" style="color:white;"> PTSA dan STMIK Sumedang</a>
</div>
<!-- Copyright -->

</footer>
<!-- Footer -->
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script>
            $(document).ready(function() {
            $('#sekolah').on('change', function() {
               var sekolahID = $(this).val();
               if(sekolahID) {
                   $.ajax({
                       url: '/getProdi/'+sekolahID,
                       type: "GET",
                       data : {"_token":"{{ csrf_token() }}"},
                       dataType: "json",
                       success:function(data)
                       {
                         if(data){
                            $('#prodi').empty();
                            $('#prodi').append('<option hidden>Pilih Prodi</option>'); 
                            $.each(data, function(key, prodi){
                                $('select[name="prodi"]').append('<option value="'+ prodi.name +'">' + prodi.name+ '</option>');
                            });
                        }else{
                            $('#prodi').empty();
                        }
                     }
                   });
               }else{
                 $('#prodi').empty();
               }
            });
            });
        </script>
<script type="text/javascript">
    $('#reload').click(function () {
        $.ajax({
            type: 'GET',
            url: 'reload-captcha',
            success: function (data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });

</script> 
</html>