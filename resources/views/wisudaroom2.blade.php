<!DOCTYPE html>
<html lang="id">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Wisuda 39 PTSA dan STMIK Sumedang - Link Zoom Wisuda Daring Room 2</title>
  <link rel="apple-touch-icon" sizes="57x57" href="{{ url('fav/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ url('fav/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ url('fav/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ url('fav/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ url('fav/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ url('fav/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ url('fav/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ url('fav/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ url('fav/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ url('fav/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ url('fav/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ url('fav/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ url('fav/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ url('fav/manifest.json') }}"> 
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ url('fav/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script></head>

<style>
        .container {
            max-width: 100%;
        }

        .formcontain {
            max-width: 500px;
            margin: 0 auto;
        }
        .reload {
            font-family: Lucida Sans Unicode
        }
    </style>
<body>
<div class="container">
    <div class="formcontain center" style="margin-top:100px;">
    <div class="row">
    <h2 class="text-center">Link Zoom Wisuda Daring Room 2 </h2>
    <h5 class="text-center">Untuk Wisudawan STIA, STIK, dan STMIK</h5>
    <h5 class="text-center">Tanggal 20 November 2021 Pukul 07.30 WIB s/d Selesai</h5>
  <br/>
  @include('message')
  <br/>

  @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
    
          <div class="row">
          <div class="col-md-12">
                  <div class="form-group mb-4">
                  <label for="exampleFormControlSelect1">MEETING ID</label>
                      <input type="text" name="meeting_id" class="form-control" placeholder="MEETING ID" value="928 2308 6273" required autofocus readonly>
                  </div>
              </div>

              <div class="col-md-12">
                  <div class="form-group">
                  <label for="exampleFormControlSelect1">MEETING PASSWORD</label>
                      <input type="text" name="meeting_password" class="form-control" placeholder="MEETING PASSWORD" value="WISUDAPTSA" required readonly>
                  </div>
              </div>
              <br/>
              <br/>
              <br/>
              <br/>
              <hr>
            
              
              </div>
          </div>   
          <div class="row">
          <div class="col-md-12 text-center">
                
                <div class="form-group" >
                  <a href="https://zoom.us/j/92823086273?pwd=YWxHQXYyamNrQ1dyRi9oUGFxWllodz09" class="btn btn-primary btn-lg" target="_blank"> <i class="fa fa-users" aria-hidden="true"></i> Join Zoom</a>
                  <a href="https://www.youtube.com/channel/UCGj68pyuFlbnvSDmo6MVrsA" class="btn btn-danger btn-lg" target="_blank"> <i class="fa fa-youtube" aria-hidden="true"></i> Join Youtube</a>
              </div>
                </div>

          </div>
          
          
    </div>
   
  </div>
</div>




</body>

</html>