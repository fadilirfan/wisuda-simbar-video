<!DOCTYPE html>
<html lang="id">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Wisuda 39 PTSA dan STMIK Sumedang - Upload Video Wisudawan</title>
  <link rel="apple-touch-icon" sizes="57x57" href="{{ url('fav/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ url('fav/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ url('fav/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ url('fav/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ url('fav/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ url('fav/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ url('fav/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ url('fav/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ url('fav/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ url('fav/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ url('fav/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ url('fav/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ url('fav/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ url('fav/manifest.json') }}"> 
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ url('fav/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"></script></head>

<style>
        .container {
            max-width: 100%;
        }

        .formcontain {
            max-width: 500px;
            margin: 0 auto;
        }
        .reload {
            font-family: Lucida Sans Unicode
        }
    </style>
<body>
<div class="container">
    <div class="jumbotron" style="margin-bottom:10px;">
    <img class="img-fluid" src="{{ url('/header.png'); }}" alt="Wisuda 39 PTSA dan STMIK Sumedang"> 
    </div>
    <div class="row">
    <div class="col-md-12">
    <h1 class="mb-4 text-center">Data Wisudawan sudah mengumpulkan Video Simbar</h1>
            <table class="table table-bordered yajra-datatable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Sekolah Tinggi</th>
                        <th>Program Studi</th>
                        <th>No Urut</th>
                        <th>Nama</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
    </div>
      </div>
    <hr>
</div>



<!-- Footer -->
<footer class="page-footer font-small pt-4" style="background-color:#2ea9e1;color:white">

  <!-- Footer Links -->
  <div class="container-fluid text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">Wisuda 39</h5>
        <p>PTSA dan STMIK Sumedang </p>
        <p>Seksi Acara dan Tim IT </p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Kontak Person</h5>

        <ul class="list-unstyled">
          <li>
            <a href="https://wa.me/085220717928" style="color:white;">Whatsapp</a>
          </li>
          <li>
            <a href="mailto:fadilirfan@stmik-sumedang.ac.id" style="color:white;">Email</a>
          </li>
          
        </ul>

      </div>
      <!-- Grid column -->


      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Menu Aplikasi</h5>

        <ul class="list-unstyled">
          <li>
            <a href="{{ url('/') }}" style="color:white;">Form Upload Simbar Wisuda</a>
          </li>
          <li>
            <a href="{{ url('/wisudawan') }}" style="color:white;">Data Wisudawan</a>
          </li>
          
        </ul>

      </div>
      <!-- Grid column -->

     

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
<div class="footer-copyright text-center py-3">© 2021 Copyright:
  <a href="#" style="color:white;"> PTSA dan STMIK Sumedang</a>
</div>
<!-- Copyright -->

</footer>
<!-- Footer -->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(function () {
          
          var table = $('.yajra-datatable').DataTable({
              processing: true,
              serverSide: true,
              responsive:true,
              ajax: "{{ route('user.index') }}",
              columns: [
                  {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                  {data: 'sekolah_tinggi', name: 'sekolah_tinggi'},
                  {data: 'jurusan', name: 'jurusan'},
                  {data: 'no_urut', name: 'no_urut'},
                  {data: 'nama', name: 'nama'},
                  {
                      data: 'action', 
                      name: 'action', 
                      orderable: true, 
                      searchable: true
                  },
              ]
          });
          
        });
    </script> 
</html>