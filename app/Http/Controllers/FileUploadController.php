<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Video;
use App\Models\Sekolah;
class FileUploadController extends Controller
{
    //
    public function index()
    {
        $sekolah = Sekolah::all();
        return view('file-upload',['sekolah' => $sekolah]);
    }

    public function store(Request $request)
    {
        
        $request->validate([
            'no_urut' => 'required',
            'nama' => 'required',
            'sekolah_tinggi' => 'required',
            'prodi' => 'required',
            'captcha' => 'required|captcha',
            'file' => 'required|mimes:mp4,Mp4,mkv,mov,Mkv,Mov|max:2000000',
        ]);
        $name = $request->file('file')->getClientOriginalName();
        $extension = $request->file('file')->getClientOriginalExtension();
        $no_urut = $request->input('no_urut');
        $nama = $request->input('nama');
        $sekolah_tinggi = $request->input('sekolah_tinggi');
        $jurusan = $request->input('prodi');
        if (($jurusan=='STIA') && ($jurusan=='STIK') && ($jurusan=='STMIK'))
        {
            $room='2';
        }else
        {
            $room='1';
        } 
        
 
        $path = $request->file('file')->storeAs('public/files/'.$sekolah_tinggi.'/'.$jurusan,$no_urut.'-'.$nama.'.'.$extension);
        $video= new Video;
 
        $video->nama = $nama;
        $video->no_urut = $no_urut;
        $video->path = $path;
        $video->sekolah_tinggi = $sekolah_tinggi;
        $video->jurusan = $jurusan;
        $video->room = $room;
        $video->save();

        return back()->with('success','Video Wisudawan Sukses Disimpan, Untuk mengeceknya dapat dilihat pada menu data wisudawan');
 
    }

    public function lihat($id)
    {
        $video = Video::findOrFail($id);
        return view('lihat',['video' => $video]);
        
    }

    public function gladiroom1()
    {
        return view('gladiroom1');
        
    }

    public function gladiroom2()
    {
        return view('gladiroom2');
        
    }

    public function wisudaroom1()
    {
        return view('wisudaroom1');
        
    }

    public function wisudaroom2()
    {
        return view('wisudaroom2');
        
    }

    public function roomtamu()
    {
        return view('roomtamu');
        
    }
}
