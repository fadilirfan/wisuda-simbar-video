<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProdiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('prodis')->insert([
            [
                'name'          => 'PBSI S1',
                'sekolah_id'   => 1
            ],
            [
                'name'          => 'Penmat S1',
                'sekolah_id'   => 1
            ],
            [
                'name'          => 'PJKR S1',
                'sekolah_id'   => 1
            ],
            [
                'name'          => 'PG PAUD S1 ',
                'sekolah_id'   => 1
            ],
            [
                'name'          => 'PGSD S1',
                'sekolah_id'   => 1
            ],
            [
                'name'          => 'PTM S1',
                'sekolah_id'   => 1
            ],
            [
                'name'          => 'PAI S1',
                'sekolah_id'   => 2
            ],
            [
                'name'          => 'Ekonomi Syariah S1',
                'sekolah_id'   => 2
            ],
            [
                'name'          => 'Sastra Inggris S1',
                'sekolah_id'   => 3
            ],
            [
                'name'          => 'Manajemen S2',
                'sekolah_id'   => 4
            ],
            [
                'name'          => 'Manajemen S1',
                'sekolah_id'   => 4
            ],
            [
                'name'          => 'Akuntansi S1',
                'sekolah_id'   => 4
            ],
            [
                'name'          => 'Ilmu Administrasi S2',
                'sekolah_id'   => 5
            ],
            [
                'name'          => 'Ilmu Administrasi Negara S1',
                'sekolah_id'   => 5
            ],
            [
                'name'          => 'Ilmu Kesehatan Masyarakat S1',
                'sekolah_id'   => 6
            ],
            [
                'name'          => 'Ilmu Keperawatan S1',
                'sekolah_id'   => 6
            ],
            [
                'name'          => 'Teknik Informatika S1',
                'sekolah_id'   => 7
            ],
            [
                'name'          => 'Sistem Informasi S1',
                'sekolah_id'   => 7
            ],
            [
                'name'          => 'Manajemen Informatika D3',
                'sekolah_id'   => 7
            ],

        ]);
    }
}
