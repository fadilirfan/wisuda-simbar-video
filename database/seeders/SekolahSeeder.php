<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SekolahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('sekolahs')->insert([
            ['name' => 'STKIP (Sekolah Tinggi Keguruan Ilmu Pendidikan)','singkatan' =>'STKIP'],
            ['name' => 'STAI (Sekolah Tinggi Agama Islam)','singkatan' =>'STAI'],
            ['name' => 'STBA (Sekolah Tinggi Bahasa Asing)','singkatan' =>'STBA'],
            ['name' => 'STIE (Sekolah Tinggi Ilmu Ekonomi)','singkatan' =>'STIE'],
            ['name' => 'STIA (Sekolah Tinggi Ilmu Administrasi)','singkatan' =>'STIA'],
            ['name' => 'STIK (Sekolah Tinggi Ilmu Kesehatan)','singkatan' =>'STIK'],
            ['name' => 'STMIK (Sekolah Tinggi Manajemen Informatika dan Komputer)','singkatan' =>'STMIK'],




        ]);
    }
}
